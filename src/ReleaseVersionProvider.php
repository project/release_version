<?php

declare(strict_types=1);

namespace Drupal\release_version;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * @todo Add class description.
 */
class ReleaseVersionProvider {
  use StringTranslationTrait;

  /**
   * The release version config.
   *
   * @var ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * Constructs a ReleaseVersionProvider object.
   */
  public function __construct(
    ConfigFactoryInterface $configFactory,
    protected readonly ModuleHandlerInterface $moduleHandler
  ) {
    $this->config = $configFactory->get('release_version.settings');
  }

  /**
   * @todo Add method description.
   */
  public function getVersion(): string {
    $key = $this->config->get('environment_variable_name');
    $version = $key && getenv($key)
      ? (string) getenv($key)
      : $this->t('Version not found');

    $this->moduleHandler->alter('release_version', $version);

    return (string) $version;
  }

}
