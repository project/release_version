<?php

namespace Drupal\release_version\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * The Release Version settings form.
 */
class ReleaseVersionSettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'release_version.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'release_version_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('release_version.settings');

    $form['environment_variable_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Environment Variable Name'),
      '#description' => $this->t('Enter the name of the environment variable name that contains the information about the current version.'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('environment_variable_name'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('release_version.settings')
      ->set('environment_variable_name', $form_state->getValue('environment_variable_name'))
      ->save();
  }

}
