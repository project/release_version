<?php

declare(strict_types=1);

namespace Drupal\release_version\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\release_version\ReleaseVersionProvider;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a version block.
 *
 * @Block(
 *   id = "release_version_version",
 *   admin_label = @Translation("Version"),
 *   category = @Translation("System"),
 * )
 */
class VersionBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Constructs the plugin instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    private readonly ReleaseVersionProvider $releaseVersionProvider,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('release_version.provider'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $build['content'] = [
      '#theme' => 'release_version_block',
      '#version' => $this->releaseVersionProvider->getVersion(),
    ];
    return $build;
  }

}
