# Release Version Drupal Module

The Release Version module places the current version number of a website
project in the toolbar.

This README file describes the configuration and use of the module.

## Setup

The setup of the Release version module is a two-step process.

1. Setting the environment variable
2. Configuring the module

First you should set the variable so it is available in your PHP environment.
If you are using an automated deployment workflow, you might want to
automatically set the variable there.

Then visit `/admin/config/release_version/settings` and enter the name of your
variable in the config form. After saving the config, the value of your version
variable should appear in the toolbar.
